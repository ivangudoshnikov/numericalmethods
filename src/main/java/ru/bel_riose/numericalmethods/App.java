package ru.bel_riose.numericalmethods;

import java.util.Iterator;
import ru.bel_riose.numericalmethods.ode.Euler;
import ru.bel_riose.numericalmethods.ode.ODEMethod;
import ru.bel_riose.numericalmethods.ode.IGetDerivativeCallback;
import java.util.LinkedList;
import ru.bel_riose.numericalmethods.ode.RungeKutta4;

/**
 * Hello world!
 *
 */
class App {

    public static void main(String[] args) {
//        exponentTestSingle(new Euler(),1,0,10,100000);
//        exponentTestSingle(new RungeKutta4(),1,0,10,100000);
        accelerationTest(new Euler(), 0, 0, 1, 100, 1000);
        accelerationTest(new RungeKutta4(), 0, 0, 1, 100, 1000);
    }
    
    /**Простая проверка метода уравнением c одной переменной x'=ax
     * 
     * @param method
     * @param a
     * @param t0
     * @param tmax
     * @param segments 
     */
    static void exponentTestSingle(ODEMethod method,final double a, double t0, double tmax,long segments){
        long startTime=System.nanoTime();
        
        double h=(tmax-t0)/(double)segments;
        
        LinkedList<Double> x0 = new LinkedList();
        x0.add(1d);

        IGetDerivativeCallback f = new IGetDerivativeCallback() {
            public Iterable<Double> getDerivative(double t, Iterable<Double> x) {
                LinkedList<Double> result = new LinkedList<Double>();
                for (Double x_i : x) {
                    result.add(a * x_i);
                }
                return result;
            }
        };

        Iterable<Double> x = x0;
        Double t = t0;
        long k=0;
        while (k<segments) {
            //System.out.println("t:"+t.toString()+" x:"+Double.toString(x.iterator().next()));
            x = method.integrate(f, t, x, h);
            t += h;
            k++;
        }
        long finishTime=System.nanoTime();
        System.out.println("Exponent test, single variable");
        System.out.println(method.getName()+":");
        System.out.println("Parameters: a="+String.valueOf(a)+", t0="+String.valueOf(t0)+", tmax="+String.valueOf(tmax)+", segments="+String.valueOf(segments)+", h="+String.valueOf(h));
        System.out.println("t:"+t.toString()+" x:"+Double.toString(x.iterator().next()));
        
        System.out.println("Precise value: "+Double.toString(x0.getFirst()*Math.exp(a*(t-t0)))+" Diff: "+Double.toString(x.iterator().next()-x0.getFirst()*Math.exp(a*(t-t0))));        
        System.out.println("Time in nanoseconds: "+String.valueOf(finishTime-startTime));
        System.out.println("");
    }
    
    /**Проверка метода уравнением x''=a, решаемого в виде системы нормального вида
     * 
     * @param method
     * @param p0
     * @param v0
     * @param a
     * @param tmax
     * @param segments 
     */
    static void accelerationTest(ODEMethod method,double p0,double v0,final double a, double tmax, long segments){
        long startTime=System.nanoTime();
        
        double h=tmax/(double)segments;
        LinkedList<Double> x0 = new LinkedList();
        x0.add(p0); //начальная координата
        x0.add(v0); //начальная скорость
        
        IGetDerivativeCallback f= new IGetDerivativeCallback() {

            public Iterable<Double> getDerivative(double t, Iterable<Double> x) {
                LinkedList<Double> result= new LinkedList<Double>();
                Iterator<Double> xIter= x.iterator();
                xIter.next();
                result.add(xIter.next()); //производная координаты - скорость
                result.add(a);                
                return result;
            }
        };
        
        Iterable<Double> x = x0;
        Double t = 0d;
        long k=0;
        while (k<segments) {
            //System.out.println("t:"+t.toString()+" x:"+Double.toString(x.iterator().next()));
            x = method.integrate(f, t, x, h);
            t += h;
            k++;
        }
        long finishTime=System.nanoTime();
        System.out.println("Acceleration");
        System.out.println(method.getName()+":");
        System.out.println("Parameters: p0:"+String.valueOf(p0)+", v0="+String.valueOf(v0)+", a="+String.valueOf(a)+", tmax="+String.valueOf(tmax)+", segments="+String.valueOf(segments)+", h="+String.valueOf(h));
        Iterator<Double> xIter=x.iterator();
        System.out.println("t:"+t.toString()+" p="+Double.toString(xIter.next())+", v="+Double.toString(xIter.next()) );
        xIter=x.iterator();
        System.out.println("Precise value: p="+Double.toString(p0+v0*tmax+a*tmax*tmax/2d)+", v="+Double.toString(v0+tmax*a));        
        System.out.println("Diff p: "+Double.toString(xIter.next()-(p0+v0*tmax+a*tmax*tmax/2d))+", v: "+Double.toString(xIter.next()-(v0+tmax*a)));
        System.out.println("Time in nanoseconds: "+String.valueOf(finishTime-startTime));
        System.out.println("");
        
    }
}
