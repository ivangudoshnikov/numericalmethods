/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.numericalmethods.kinematics2d;

import ru.bel_riose.geometry.Vector;

/**
 *
 * @author Ivan Gudoshnikov
 */
public interface IGetAccelerationCallback {
    public Iterable<Vector> getAcceleration(double t,Iterable<Vector> pos, Iterable<Vector> speed);   
    
}
