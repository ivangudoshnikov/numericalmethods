/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.numericalmethods.kinematics2d;

import ru.bel_riose.geometry.Vector;
import java.util.Iterator;
import java.util.LinkedList;
import ru.bel_riose.numericalmethods.ode.IGetDerivativeCallback;
import ru.bel_riose.numericalmethods.ode.ODEMethod;

/**
 *
 * @author Ivan Gudoshnikov
 */
public class ODEWrapSolver extends PhysicsSolver {

    private ODEMethod method;

    public ODEWrapSolver(ODEMethod method) {
        this.method = method;
    }

    @Override
    public String getName() {
        return getName(method.getName());
    }
    
    public static String getName(String wrapped){
        return wrapped+" wrapper";
    }

    @Override
    public void update(double dt, Iterable<? extends Point> objects, final IGetAccelerationCallback a) {
        LinkedList<Double> x0 = new LinkedList<Double>();
        for (Point p : objects) {
            Vector pos = p.getPos(), speed = p.getSpeed();
            x0.add(pos.x);
            x0.add(pos.y);
            x0.add(speed.x);
            x0.add(speed.y);
        }
        IGetDerivativeCallback f = new IGetDerivativeCallback() {
            @Override
            public Iterable<Double> getDerivative(double t, Iterable<Double> x) {
                LinkedList<Vector> posList = new LinkedList<Vector>(), speedList = new LinkedList<Vector>();
                Iterator<Double> xIter = x.iterator();
                while (xIter.hasNext()) {
                    //сначала координаты
                    posList.add(new Vector(xIter.next(), xIter.next()));
                    //потом скорости
                    speedList.add(new Vector(xIter.next(), xIter.next()));
                }
                Iterable<Vector> accelList = a.getAcceleration(t, posList, speedList);
                Iterator<Vector> speedIter = speedList.iterator();
                LinkedList<Double> f = new LinkedList<Double>();
                for (Vector accel : accelList) {
                    //производная координат - скорость
                    Vector speed = speedIter.next();
                    f.add(speed.x);
                    f.add(speed.y);
                    //производная скорости - ускорение
                    f.add(accel.x);
                    f.add(accel.y);
                }
                return f;
            }
        };
        Iterator<Double> newValuesIter = method.integrate(f, 0, x0, dt).iterator();
        for (Point p : objects) {
            //сначала считываем вектор координат
            p.setPos(new Vector(newValuesIter.next(), newValuesIter.next()));
            //а потом скорости
            p.setSpeed(new Vector(newValuesIter.next(), newValuesIter.next()));
        }
    }
}
