/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.numericalmethods.kinematics2d;

import java.io.Serializable;

/**
 *
 * @author Ivan Gudoshnikov
 */
public abstract class PhysicsSolver implements Serializable{

    public abstract String getName();

    public abstract void update(double dt, Iterable<? extends Point> objects,IGetAccelerationCallback a);    
}
