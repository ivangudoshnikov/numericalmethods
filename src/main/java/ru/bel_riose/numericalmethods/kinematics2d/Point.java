/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.numericalmethods.kinematics2d;

import ru.bel_riose.geometry.Vector;

/**Отмеченная точка, имеет местоположение и скорость
 *
 * @author Ivan Gudoshnikov
 */
public interface Point {
    public Vector getPos();
    public Vector getSpeed();
    public void setPos(Vector v);
    public void setSpeed(Vector v);
}
