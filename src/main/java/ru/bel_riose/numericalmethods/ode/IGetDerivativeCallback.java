package ru.bel_riose.numericalmethods.ode;


/**Функциональный интерфейс, служет для задания функции, стоящей в правой части уравнения x'=f(t,x) 
 * 
 * @author Ivan Gudoshnikov
 */
public interface IGetDerivativeCallback {
    /**Собственно функция f
     * Для хранения предстваления вектора используется интерфейс Iterable, самая простая его реализация - LinkedList
     * @param t
     * @param x
     * @return f(t,x) - порядок и число компонент должно соответствовать вектору x.
     */
    Iterable<Double> getDerivative(double t, Iterable<Double> x);    
}
