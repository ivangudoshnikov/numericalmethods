/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.numericalmethods.ode;

import java.io.Serializable;


/**Ошибка несоответствия числа компонент вектора
 * 
 * @author Ivan Gudoshnikov
 */
class WrongAmountOfVariablesException extends RuntimeException{};

/**Абстрактный класс, задающий интерфейс для численных методов, решающих
 * систему ОДУ x'=f(t,x)
 * с начальным условием x(t0)=x0,
 * и шагом h
 * 
 * @author Ivan
 */
public abstract class ODEMethod implements Serializable{
    /**Считает один шаг метода
     * 
     * @param f правая часть
     * @param t0 начальный момент времени
     * @param x0 начальное условие
     * @param h промежуток(шаг)
     * @return x(t0+h)
     */
    public abstract Iterable<Double> integrate(IGetDerivativeCallback f,double t0, Iterable<Double> x0, double h);    
    /**
     * @return название данного метода 
     */
    public abstract String getName();
}
