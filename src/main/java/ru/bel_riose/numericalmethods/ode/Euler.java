/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.numericalmethods.ode;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**Реализация метода Эйлера
 *
 * @author Ivan Gudoshnikov
 */
public class Euler extends ODEMethod {
    public static final String name="Euler";
    
    @Override
    public String getName(){
        return name;
    
    }

    @Override
    public Iterable<Double> integrate(IGetDerivativeCallback f, double t0, Iterable<Double> x0, double h) {
        LinkedList<Double> result = new LinkedList<Double>();
        Iterator<Double> derivative = f.getDerivative(t0, x0).iterator();
        try {
            for (double x_i : x0) {
                //x_i = x_i + h*f(t0,x_1,..,x_n)
                result.add(x_i + h * derivative.next());
            }
        } catch (NoSuchElementException e) {
            throw new WrongAmountOfVariablesException();
        };
        if (derivative.hasNext()) {
            throw new WrongAmountOfVariablesException();
        }
        return result;
    }
}
