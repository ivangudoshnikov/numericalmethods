/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.numericalmethods.ode;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**Реализация метода Рунге-Кутта 4 порядка
 *
 * @author Ivan Gudoshnikov
 */
public class RungeKutta4 extends ODEMethod {
    public static final String name="RungeKutta4";
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public Iterable<Double> integrate(IGetDerivativeCallback f, double t0, Iterable<Double> x0, double h) {
        try {
            // Вычислем k1=h*f(t0,x0), и аргумент для следующего запроса производной arg2=x0+k1/2
            LinkedList<Double> k1 = new LinkedList(), arg = new LinkedList<Double>();
            Iterable<Double> derivative = f.getDerivative(t0, x0);
            Iterator<Double> x0iter = x0.iterator();

            for (double f1 : derivative) {
                k1.add(h * f1);
                arg.add(x0iter.next() + k1.getLast() / 2d);
            }
            if (x0iter.hasNext()) {
                throw new WrongAmountOfVariablesException();
            }

            //k2=h*f(t0+h/2,arg2), arg3=x0+k2/2
            LinkedList<Double> k2 = new LinkedList<Double>();
            derivative = f.getDerivative(t0 + h / 2d, arg);
            arg.clear();
            x0iter = x0.iterator();

            for (double f2 : derivative) {
                k2.add(h * f2);
                arg.add(x0iter.next() + k2.getLast() / 2d);
            }
            if (x0iter.hasNext()) {
                throw new WrongAmountOfVariablesException();
            }

            //k3=h*f(t0+h/2,arg3), arg4=x0+k3
            LinkedList<Double> k3 = new LinkedList<Double>();
            derivative = f.getDerivative(t0 + h / 2d, arg);
            arg.clear();
            x0iter = x0.iterator();

            for (double f3 : derivative) {
                k3.add(h * f3);
                arg.add(x0iter.next() + k3.getLast());
            }
            if (x0iter.hasNext()) {
                throw new WrongAmountOfVariablesException();
            }

            //k4=h*f(t0+h,arg4)
            LinkedList<Double> k4 = new LinkedList<Double>();
            derivative = f.getDerivative(t0 + h, arg);

            for (double f4 : derivative) {
                k4.add(h * f4);
            }
            if (x0iter.hasNext()) {
                throw new WrongAmountOfVariablesException();
            }

            //x1=x0+(k1+2*k2+2*k3+k4)/6
            LinkedList<Double> result = new LinkedList<Double>();
            x0iter = x0.iterator();
            Iterator<Double> k2iter = k2.iterator(), k3iter = k3.iterator(), k4iter = k4.iterator();
            for (Double k1_i : k1) {
                result.add(x0iter.next() + (k1_i + 2d * (k2iter.next() + k3iter.next()) + k4iter.next()) / 6d);
            }
            if (x0iter.hasNext() || k2iter.hasNext() || k3iter.hasNext() || k4iter.hasNext()) {
                throw new WrongAmountOfVariablesException();
            }

            return result;
        } catch (NoSuchElementException e) {
            throw new WrongAmountOfVariablesException();
        }
    }
}
